#include "Bishop.h"
#include "Board.h"
#include <cmath>
#include <iostream>
Bishop::Bishop(Position pos, bool isWhite, char type) : Piece(pos, isWhite, type)
{
}

bool Bishop::canMove(Position pos, Piece* board[8][8])
{
    int dx = pos.getX() - this->getPos().getX();
    int dy = pos.getY() - this->getPos().getY();
    if ((abs(dx) - abs(dy) == 0))
    {
        int stepX = (dx > 0) - (dx < 0);
        int stepY = (dy > 0) - (dy < 0);
        int currentX = this->getPos().getX() + stepX;
        int currentY = this->getPos().getY() + stepY;
        while (currentX != pos.getX() && currentY != pos.getY())
        {
            if (board[currentX][currentY] != nullptr)
                return false;

            currentX += stepX;
            currentY += stepY;
        }

        return true;
    }

    return false;
}

Bishop::~Bishop()
{
}
