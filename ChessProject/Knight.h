#pragma once
#include "Piece.h"

#ifndef KNIGHT_H
#define KNIGHT_H
class Knight : public Piece {
public:
    Knight(Position pos, bool isWhite, char type);
    bool canMove(Position pos, Piece* board[8][8]);
};

#endif