#include "Board.h"

#include <iostream>

Board::Board() {
	for (int i = 0; i < 8; ++i) {
		for (int j = 0; j < 8; ++j) {
			board[i][j] = nullptr;
		}
	}
	for (int i = 0; i < 8; ++i) {
		board[1][i] = new Pawn(Position(1, i), false, 'p');
		board[6][i] = new Pawn(Position(6, i), true, 'P');
	}

	board[0][2] = new Bishop(Position(0, 2), false, 'b');
	board[0][5] = new Bishop(Position(0, 5), false, 'b');
	board[7][2] = new Bishop(Position(7, 2), true, 'B');
	board[7][5] = new Bishop(Position(7, 5), true, 'B');
	board[0][1] = new Knight(Position(0, 1), false, 'n');
	board[0][6] = new Knight(Position(0, 6), false, 'n');
	board[7][1] = new Knight(Position(7, 1), true, 'N');
	board[7][6] = new Knight(Position(7, 6), true, 'N');
	board[7][3] = new Queen(Position(7, 3), true, 'Q');
	board[0][3] = new Queen(Position(0, 3), false, 'q');
	board[0][4] = new King(Position(0, 4), false, 'k');
	board[7][4] = new King(Position(7, 4), true, 'K');
	board[0][0] = new Rook(Position(0, 0), false, 'r');
	board[0][7] = new Rook(Position(0, 7), false, 'r');
	board[7][0] = new Rook(Position(7, 0), true, 'R');
	board[7][7] = new Rook(Position(7, 7), true, 'R');

}

Board::~Board() {
	for (int i = 0; i < 8; ++i) {
		for (int j = 0; j < 8; ++j) {
			delete board[i][j];
		}
	}
}



char Board::getPiece(Position pos) const
{
	Piece* currentPiece = board[pos.getX()][pos.getY()];
	if (currentPiece != nullptr && currentPiece->getPos() == pos) {
		return currentPiece->getType();
	}

	return '#';
}


std::string Board::getBoardString() const {
	std::string result;

	for (int i = 0; i < 8; ++i) {
		for (int j = 0; j < 8; ++j) {
			Position currentPos(i, j);
			char pieceChar = getPiece(currentPos);

			result += pieceChar;

			result += ' ';
		}


		result += '\n';
	}

	return result;
}

Position Board::Convertor(const std::string stringPos)
{
	int x_source = int(stringPos[0]) - 'a'; // Convert char 'a' to 0, 'b' to 1, and so on
	int y_source = 8 - (stringPos[1] - '0'); // Convert char '1' to 7, '2' to 6, and so on
	return Position(y_source, x_source);
}





int Board::move(Position fromPos, Position toPos)
{
	Piece* piece = board[fromPos.getX()][fromPos.getY()];

	// Check if there is a piece at the source position
	if (piece == nullptr)
		return 2;  // No piece at the source position

	bool isWhiteTurn = (this->countMoves % 2 == 0);
	Piece* targetPiece = board[toPos.getX()][toPos.getY()];

	// Check if the source and target positions are the same
	if (fromPos == toPos)
		return 7;  // 'fromPos' and 'toPos' are the same

	// Check if the piece at the source position belongs to the current player
	if ((isWhiteTurn && !piece->getIsWhite()) || (!isWhiteTurn && piece->getIsWhite()))
	{
		return 2;  // Wrong player's turn
	}

	// Check if the target square is occupied by a piece of the same color
	if (targetPiece != nullptr && ((targetPiece->getIsWhite() && isWhiteTurn) || (!targetPiece->getIsWhite() && !isWhiteTurn)))
	{
		return 3;  // Invalid move - target square is occupied by a piece of the same color
	}

	// Check if the move is valid
	if (piece->canMove(toPos, board))
	{
		// Perform the move
		board[toPos.getX()][toPos.getY()] = piece;
		board[fromPos.getX()][fromPos.getY()] = nullptr;
		piece->setPos(toPos);

		// Check if the move results in a check
		if (checkIfCheck(isWhiteTurn))
		{
			// Revert the move if it results in a check
			board[fromPos.getX()][fromPos.getY()] = piece;
			board[toPos.getX()][toPos.getY()] = targetPiece;
			piece->setPos(fromPos);
			return 4;  // Move results in a check
		}
		else
		{
			// Move successful, and no check for the opponent
			this->countMoves++;

			// Check if the opponent is in check after the move
			if (checkIfCheck(!isWhiteTurn))
				return 1;  // The opponent is in check after the move
			else
				return 0;  // Move successful
		}
	}
	else
	{
		return 6;  // Piece cannot move to the target square
	}
}


bool Board::checkIfCheck(bool isWhite)
{
	int x = 0, y = 0;
	if (isWhite)
	{
		Position posOfKing = findPiecePosition('K');
		for (int i = 0; i < 8; ++i)
		{
			for (int j = 0; j < 8; ++j)
			{
				if (board[i][j] != nullptr)
				{
					if (std::islower(getPiece(Position(i, j)))) {
						if (board[i][j]->canMove(posOfKing, board))
						{
							return true;
						}
					}

				}
			}
		}

	}
	else
	{
		Position posOfKing = findPiecePosition('k');
		for (int i = 0; i < 8; ++i)
		{
			for (int j = 0; j < 8; ++j)
			{
				if (board[i][j] != nullptr)
				{
					if (!std::islower(getPiece(Position(i, j)))) {
						if (board[i][j]->canMove(posOfKing, board))
						{
							return true;
						}
					}

				}
			}
		}
	}
	return false;
}

Position Board::findPiecePosition(char pieceSymbol) const
{
	for (int i = 0; i < 8; ++i)
	{
		for (int j = 0; j < 8; ++j)
		{
			if (getPiece(Position(i, j)) == pieceSymbol)
			{
				return Position(i, j);
			}
		}
	}
}
