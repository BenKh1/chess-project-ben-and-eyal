#include "Piece.h"

#ifndef ROOK_H
#define ROOK_H
class Rook : public Piece {
public:
	Rook(Position pos, bool isWhite, char type);
	bool canMove(Position pos, Piece* board[8][8]);
	~Rook();
};

#endif // ROOK_H