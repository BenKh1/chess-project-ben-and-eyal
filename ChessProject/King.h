#pragma once
#ifndef KING_H
#define KING_H
#include "Piece.h"
#include "Board.h"
class King:public Piece {
public:
	King(Position pos, bool isWhite, char type);
	bool canMove(Position pos, Piece* board[8][8]);
	bool canCastle(bool isWhite);
};

#endif