#include "Piece.h"

Piece::Piece(Position pos, bool isWhite, char type): pos(pos), isWhite(isWhite), type(type) {
}

bool Piece::getIsWhite() const {
    return this->isWhite;
}

char Piece::getType() const {
    return this->type;
}


Position Piece::getPos() const {
    return this->pos;
}

void Piece::setPos(Position pos)
{
    this->pos = pos;
}
