#pragma once
#ifndef PAWN_H
#define PAWN_H
#include "Piece.h"
class Pawn : public Piece {
public:
    Pawn(Position pos, bool isWhite, char type);
    bool canMove(Position pos, Piece* board[8][8]);
    bool hasMoved();
};

#endif