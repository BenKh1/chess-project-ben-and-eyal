#pragma once
#include "Piece.h"

#ifndef BISHOP_H
#define BISHOP_H
class Bishop : public Piece {
public:
	Bishop(Position pos, bool isWhite, char type);
	bool canMove(Position pos, Piece* board[8][8]);
	~Bishop();
};

#endif