#pragma once
#ifndef BOARD_H
#define BOARD_H
class Piece;
#include "Position.h"
#include "Pawn.h"
#include "King.h"
#include "Rook.h"
#include "Bishop.h"
#include "Knight.h"
#include "Queen.h"
#include <string>

class Board {
private:
    Piece* board[8][8];
    int countMoves = 0;

public:
    Board();
    ~Board();
    int move(Position fromPos, Position toPos);
    char getPiece(Position pos) const;
    std::string getBoardString() const;
    Position Convertor(std::string pos);
    bool checkIfCheck(bool isWhite);
    Position findPiecePosition(char pieceSymbol) const;
};
#endif 
