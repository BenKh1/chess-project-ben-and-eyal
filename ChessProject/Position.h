#pragma once
#ifndef POSITION_H
#define POSITION_H
class Position {
private:
	int _x;
	int _y;
public:
	Position(int x, int y);
	int getX() const;  // Add this getter method
	int getY() const;  // Add this getter method
	bool operator==(const Position& other) const;
};

#endif