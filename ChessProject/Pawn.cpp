#include "Pawn.h"
#include "Piece.h" 
#include <cmath>
#include <iostream>
Pawn::Pawn(Position pos, bool isWhite, char type) : Piece(pos, isWhite, type) {

}


bool Pawn::canMove(Position pos, Piece* board[8][8])
{



    bool _hasmoved = hasMoved();
    if (getIsWhite()==true) {
        int dx = getPos().getX() - pos.getX()  ;
        int dy = getPos().getY() - pos.getY()  ;
        dy = abs(dy);
        if (dx == 2 && !(_hasmoved)) {
            int stepX = (dx < 0) - (dx > 0);
            int currentX = getPos().getX() + stepX;
            std::cout << currentX;
            while (currentX != pos.getX())
            {
                if (board[currentX][getPos().getY()] != nullptr) {

                    return false;
                }


                currentX += stepX;
            }

            return true;
        }
        if (dx == 1) {

            if (dy == 1 && dx == 1) {
                if (board[pos.getX()][pos.getY()] != nullptr) {
                    return true;
                }
                return false;
            }

            if (board[pos.getX()][pos.getY()] != nullptr) {
                return false;
            }

            return true;
        }
        return false;
    }
    else {
        int dx = pos.getX()- getPos().getX();
        int dy = getPos().getY() - pos.getY();
        dy = abs(dy);
        if (dx == 2 && !(_hasmoved)) {
            int stepX = (dx > 0) - (dx < 0);
            int currentX = getPos().getX() + stepX;
            std::cout << currentX;
            while (currentX != pos.getX())
            {
                if (board[currentX][getPos().getY()] != nullptr) {

                    return false;
                }


                currentX += stepX;
            }

            return true;
        }
        if (dx == 2 && _hasmoved) {

            return false;
        }
        if (dx == 1) {

            if (dy == 1 && dx == 1) {
                if (board[pos.getX()][pos.getY()] != nullptr) {
                    return true;
                }
                return false;
            }

            if (board[pos.getX()][pos.getY()] != nullptr) {
                return false;
            }

            return true;
        }
        return false;
    }


}



bool Pawn::hasMoved()
{
    if (!getIsWhite()) {
        if (getPos().getX()==1) {

            return false;
        }
        
    }
    else {
        if (getPos().getX() == 6) {

            return false;
        }
    }
    return true;
}
