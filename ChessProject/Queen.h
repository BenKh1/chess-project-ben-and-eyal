#include "Piece.h"
#include "Rook.h"
#include "Bishop.h"
#ifndef QUEEN_H
#define QUEEN_H
class Queen : public Piece {
public:
    Queen(Position pos, bool isWhite, char type);
    bool canMove(Position pos, Piece* board[8][8]);
};

#endif 