#include "King.h"
#include <cmath>
#include <iostream>

King::King(Position pos, bool isWhite, char type) : Piece(pos, isWhite, type) 
{
}

bool King::canMove(Position pos, Piece* board[8][8])
{
    int deltaX = std::abs(pos.getX() - getPos().getX());
    int deltaY = std::abs(pos.getY() - getPos().getY());

    if ((deltaX == 1 && deltaY==0 || deltaY == 1&& deltaX == 0) || (deltaX == 1 && deltaY == 1)) {
        return true;
    }
       
    return false;
}


bool King::canCastle(bool isWhite)
{
	return false;
}
