#include "Knight.h"
#include "Board.h"
#include <cmath>
#include <iostream>
Knight::Knight(Position pos, bool isWhite, char type) : Piece(pos, isWhite, type)
{
}

bool Knight::canMove(Position pos, Piece* board[8][8])
{
    int dx = pos.getX() - this->getPos().getX();
    int dy = pos.getY() - this->getPos().getY();
    if (((abs(dx) == 2 && abs(dy) == 1) || ((abs(dy) == 2 && abs(dx) == 1))))
    {
        return true;
    }

    return false;
}