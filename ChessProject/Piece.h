#pragma once
#ifndef PIECE_H
#define PIECE_H
#include "Position.h"
class Board;

class Piece {
private:
    Position pos;
    char type;
    bool isWhite; 
public:
    Piece(Position pos, bool isWhite, char type);
    Position getPos() const;
    void setPos(Position pos);
    char getType() const;
    bool getIsWhite() const;
    virtual bool canMove(Position pos, Piece* board[8][8]) = 0;
};

#endif
