#include "Queen.h"
#include <cmath>
Queen::Queen(Position pos, bool isWhite, char type) : Piece(pos, isWhite, type)
{
}

bool Queen::canMove(Position pos, Piece* board[8][8])

{
    Rook* tempRook= new Rook(getPos(), true, 'R');
    Bishop* tempBishop= new Bishop(getPos(), true, 'B');


    bool validRookMove = tempRook->canMove(pos, board);
    bool validBishopMove = tempBishop->canMove(pos, board);

    if (validRookMove || validBishopMove)
    {
        return true;
    }
    else
    {
        return false;
    }
}
