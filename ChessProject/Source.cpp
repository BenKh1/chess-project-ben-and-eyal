/*
This file servers as an example of how to use Pipe.h file.
It is recommended to use the following code in your project, 
in order to read and write information from and to the Backend
*/

#include "Pipe.h"
#include <iostream>
#include <thread>
#include "Board.h"
using std::cout;
using std::endl;
using std::string;


void main()
{
	srand(time_t(NULL));

	
	Pipe p;
	bool isConnect = p.connect();
	
	string ans;
	while (!isConnect)
	{
		cout << "cant connect to graphics" << endl;
		cout << "Do you try to connect again or exit? (0-try again, 1-exit)" << endl;
		std::cin >> ans;

		if (ans == "0")
		{
			cout << "trying connect again.." << endl;
			Sleep(5000);
			isConnect = p.connect();
		}
		else 
		{
			p.close();
			return;
		}
	}
	

	char msgToGraphics[1024];
	// msgToGraphics should contain the board string accord the protocol
	// YOUR CODE

	strcpy_s(msgToGraphics, "rnbqkbnrpppppppp################################PPPPPPPPRNBQKBNR0"); // just example...
	
	p.sendMessageToGraphics(msgToGraphics);   // send the board string
	int code;
	char codeArray[2];
	// get message from graphics
	string msgFromGraphics = p.getMessageFromGraphics();
	Board* board = new Board();
	while (msgFromGraphics != "quit")
	{
		Position fromPos = board->Convertor(msgFromGraphics.substr(0, 2));
		std::cout << fromPos.getX() << "," << fromPos.getY() << endl;

		Position toPos = board->Convertor(msgFromGraphics.substr(2, 2));

		code = board->move(fromPos, toPos);
		std::cout << board->getBoardString() << endl;
		std::cout << code << endl;
		codeArray[0] = static_cast<char>(code + '0'); // Convert the result code to char
		codeArray[1] = '\0'; // Null-terminate the string

		strcpy_s(msgToGraphics, codeArray); // msgToGraphics should contain the result of the operation
		p.sendMessageToGraphics(msgToGraphics); // return result to graphics


		// get message from graphics
		msgFromGraphics = p.getMessageFromGraphics();
	}

	p.close();
}